import { omit } from 'ramda';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import {
  ItemListProps,
  ListProps,
  ListHasUsersProps,
  ListsProps,
  ListFindProps,
} from 'src/interfaces/list.interface';
import { Lists } from 'src/classes/list/list.class';
import { Items } from 'src/classes/list/itemList.class';
import { ListHasUsers } from 'src/classes/list/listHasUsers.class';

@Injectable()
export class ListService {
  constructor(
    @InjectRepository(ListHasUsers)
    private readonly listHasUsersRepository: Repository<ListHasUsersProps>,
    @InjectRepository(Items)
    private readonly itemsRepository: Repository<ItemListProps>,
    @InjectRepository(Lists)
    private readonly listRepository: Repository<ListProps>,
  ) {}

  async createList(data: ListProps, userId: string): Promise<ListProps> {
    const list = await this.listRepository.save(data);
    await this.listHasUsersRepository.save({ listId: list.id, userId });
    return list;
  }

  async createItemList(data: ItemListProps): Promise<ItemListProps> {
    const containsList = await this.listRepository
      .find({
        id: data.listId,
      })
      .then((resp) => resp.length > 0);

    if (!containsList) {
      throw 'Lista não existe';
    }

    return await this.itemsRepository.save(data);
  }

  async updateList(data: ListProps): Promise<void> {
    await this.listRepository.update({ id: data.id }, omit(['id'], data));
  }

  async shareList(id: string, userIdShare: string): Promise<void> {
    const containsList = await this.listHasUsersRepository
      .find({
        listId: id,
        userId: userIdShare,
      })
      .then((resp) => resp.length > 0);
    !containsList &&
      (await this.listHasUsersRepository.save({
        listId: id,
        userId: userIdShare,
      }));
  }

  async deleteList(id: string, userIdShare: string): Promise<void> {
    const containsList = await this.listHasUsersRepository
      .find({
        listId: id,
      })
      .then((resp) => resp.length <= 1);

    await this.listHasUsersRepository.delete({
      listId: id,
      userId: userIdShare,
    });

    if (containsList) {
      await this.itemsRepository.delete({ listId: id });

      await this.listRepository.delete(id);
    }
  }

  async updateItemList(data: ItemListProps): Promise<void> {
    await this.itemsRepository.update({ id: data.id }, omit(['id'], data));
  }

  async findOneList(condition: ListFindProps): Promise<ListsProps | any> {
    const list = await this.listRepository.findOne(condition);
    await this.itemsRepository
      .query(
        `SELECT id, name, quantity, price FROM items WHERE items."listId" = '${list.id}'`,
      )
      .then((resp) => (list['items'] = resp));

    return list;
  }

  async findAllLists(idUser: string): Promise<ListsProps | any> {
    const lists: Array<ListProps | ListsProps> = await this.listRepository
      .query(
        'SELECT li.id, li.name, li.description FROM lists as li ' +
          'INNER JOIN "listHasUsers" as lu ON lu."listId" = li.id ' +
          `WHERE lu."userId" = '${idUser}'`,
      )
      .then((resp) => resp);

    const listIds: Array<string> = [];
    lists.forEach((e) => listIds.push(e.id));

    for (const idx in listIds) {
      await this.itemsRepository
        .query(
          `SELECT id, name, quantity, price FROM items WHERE items."listId" = '${listIds[idx]}'`,
        )
        .then((resp) => (lists[idx]['items'] = resp));
    }

    return lists;
  }

  async deleteItemList(id: string): Promise<void> {
    await this.itemsRepository.delete(id);
  }
}
