import { createTransport, Transporter, SentMessageInfo } from 'nodemailer';
import * as hbs from 'nodemailer-express-handlebars';

export abstract class Mailer {
  static useMailer() {
    const transportMailer: Transporter<SentMessageInfo> = createTransport({
      pool: true,
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: process.env.SEND_EMAIL,
        pass: process.env.SEND_EMAIL_PASS,
      },
    });

    const hbsConfig = {
      viewEngine: {
        extName: '.hbs',
        partialsDir: 'src/services/email/templates',
        layoutsDir: 'src/services/email/templates',
        defaultLayout: '',
      },
      viewPath: 'src/services/email/templates',
      extName: '.hbs',
    };

    return transportMailer.use('compile', hbs(hbsConfig));
  }
}
