import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { BlackListProps } from 'src/interfaces/blacklist.interface';
import { ResetPasswordBlackList } from 'src/classes/blacklist/resetPasswordBlacklist.class';

@Injectable()
export class ResetPasswordBlacklistService {
  constructor(
    @InjectRepository(ResetPasswordBlackList)
    private readonly blackList: Repository<BlackListProps>,
  ) {}

  async create(hash: string): Promise<BlackListProps> {
    return await this.blackList.save({ hash });
  }

  async findOne(condition: BlackListProps): Promise<BlackListProps> {
    return await this.blackList.findOne(condition);
  }

  async removeAllToDate(date: Date): Promise<any> {
    return await this.blackList
      .createQueryBuilder()
      .delete()
      .from('resetPasswordBlackList')
      .where('"creationDate" < :date', { date })
      .execute();
  }
}
