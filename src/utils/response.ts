export interface send {
  success: boolean;
  message: string;
  data?: any;
}

export const sendMethods = ({ message, data, success }: send) => ({
  success,
  message,
  data,
});
