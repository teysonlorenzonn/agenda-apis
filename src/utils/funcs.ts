import { isNil } from 'ramda';
import * as bcrypt from 'bcrypt';

export const generateCrptyPass = async (pass: string) => {
  const salt = await bcrypt.genSalt();
  return await bcrypt.hash(pass, salt);
};

export const compareCrptyPass = async (pass: string, hash: string) => {
  return await bcrypt.compare(pass, hash);
};

export const normalizeInserts = (data: any) => {
  const normalize = {};
  for (const key in data) {
    if (!isNil(data[key])) {
      normalize[key] = data[key];
    }
  }
  return normalize;
};

export const genericError = 'Erro na comunicação com o banco de dados';

export const validJwt = async (
  auth: any,
  jwtService: any,
  hash?: string,
): Promise<any> => {
  let data = undefined;
  const cookie = !hash ? auth : hash;
  if (cookie) {
    data = await jwtService.verifyAsync(cookie);
  }
  return data;
};
