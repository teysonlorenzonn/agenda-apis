export interface BlackListProps {
  id?: string;
  hash: string;
  creationDate?: Date;
}
