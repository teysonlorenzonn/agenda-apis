export interface UserProps {
  id?: string;
  firstName: string;
  lastName: string;
  phoneNumber?: number;
  email: string;
  password: string;
  userName: string;
  lastModifyDate: Date;
  creationDate: Date;
}

export interface PasswordProps {
  password: string;
  currentPassword: string;
  confirmPassword: string;
  hash?: string;
}

export interface UserUpdateProps {
  id?: string;
  firstName?: string;
  lastName?: string;
  phoneNumber?: number;
  email?: string;
  password?: string;
  userName?: string;
}
