export interface ListProps {
  id?: string;
  name: string;
  description: string;
  creationDate?: Date;
  lastModifyDate?: Date;
}

export interface ItemListProps {
  id?: string;
  name: string;
  quantity: number;
  creationDate?: Date;
  lastModifyDate?: Date;
  listId: string;
  price?: string;
}

export interface ListsProps {
  id?: string;
  name: string;
  description: string;
  items: Array<ItemListProps> | [];
}

export interface ListHasUsersProps {
  id?: string;
  listId: string;
  userId: string;
}

export interface ListFindProps {
  id?: string;
  name?: string;
  description?: string;
  creationDate?: Date;
  lastModifyDate?: Date;
}
