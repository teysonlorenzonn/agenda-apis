import { Body, Controller, Get, Post, Req, Res, Put } from '@nestjs/common';
import { omit, isNil } from 'ramda';
import { JwtService } from '@nestjs/jwt';
import { Response, Request } from 'express';

import { Mailer } from 'src/services/email/mailer.service';
import { validJwt } from 'src/utils/funcs';
import { send } from 'src/utils/response';
import { UserService } from 'src/services/user/user.service';
import { ResetPasswordBlacklistService } from 'src/services/blacklist/resetPasswordBlacklist.service';
import { generateCrptyPass, compareCrptyPass } from 'src/utils/funcs';
import { PasswordProps, UserProps } from 'src/interfaces/user.interface';
import { sendMethods } from 'src/utils/response';
import { ListService } from 'src/services/list/list.service';

@Controller('user')
export class UserController {
  constructor(
    private readonly listService: ListService,
    private readonly userService: UserService,
    private readonly blackList: ResetPasswordBlacklistService,
    private jwtService: JwtService,
  ) {}

  @Get()
  async user(
    @Req() request: Request,
    @Res({ passthrough: true }) response: Response,
  ) {
    let resp: send;
    let statusCode = 200;

    try {
      let auth = request.headers['authorization'];
      if (!auth) {
        auth = request.cookies['token'];
      }
      const data = await validJwt(auth, this.jwtService);

      if (!data) {
        return response.status(401).send(
          sendMethods({
            message: 'Sessão expirada',
            success: false,
          }),
        );
      }

      const user = await this.userService.findOne({ id: data.id });

      resp = sendMethods({
        message: 'Usuário consultado com sucesso',
        success: true,
        data: omit(['password'], user),
      });
    } catch (e) {
      statusCode = 403;
      resp = sendMethods({
        message: 'Sessão expirada',
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }

  @Put()
  async updateUser(
    @Body() body: UserProps,
    @Res() response: Response,
    @Req() request: Request,
  ) {
    let resp: send;
    let statusCode = 202;
    try {
      let auth = request.headers['authorization'];
      if (!auth) {
        auth = request.cookies['token'];
      }
      const data = await validJwt(auth, this.jwtService);
      if (!data) {
        return response.status(401).send(
          sendMethods({
            message: 'Sessão expirada',
            success: false,
          }),
        );
      }

      if (!isNil(body.password)) {
        return response.status(200).send(
          sendMethods({
            message: 'Não é possível alterar a senha',
            success: false,
          }),
        );
      }

      await this.userService.update(data.id, body);

      resp = sendMethods({
        message: 'Usuário atualizado com sucesso',
        success: true,
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }

  @Post()
  async createUser(@Body() body: UserProps, @Res() response: Response) {
    let resp: send;
    let statusCode = 201;
    try {
      if (isNil(body.password)) {
        return response.status(200).send(
          sendMethods({
            message: 'Usuário precisa de uma senha',
            success: false,
          }),
        );
      }
      const hashedPassword = await generateCrptyPass(body.password);
      const user = await this.userService.create({
        ...body,
        password: hashedPassword,
      });

      resp = sendMethods({
        message: 'Usuário criado com sucesso',
        success: true,
        data: omit(['password'], user),
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }

  @Put('password')
  async resetPassword(
    @Body() body: PasswordProps,
    @Res() response: Response,
    @Req() request: Request,
  ) {
    let resp: send;
    let statusCode = 200;
    try {
      if (body.hash && (await this.blackList.findOne({ hash: body.hash }))) {
        return response.status(200).send(
          sendMethods({
            message: 'Senha já foi atualizada com o hash atual',
            success: false,
          }),
        );
      }

      let auth = request.headers['authorization'];
      if (!auth) {
        auth = request.cookies['token'];
      }
      const data = await validJwt(auth, this.jwtService, body.hash);
      if (!data) {
        return response.status(401).send(
          sendMethods({
            message: 'Sessão expirada',
            success: false,
          }),
        );
      }

      const { password } = await this.userService.findCurrentPassword(data.id);

      if (
        body.hash ||
        (await compareCrptyPass(body.currentPassword, password))
      ) {
        if (body.password === body.confirmPassword) {
          if (!(await compareCrptyPass(body.password, password))) {
            const hashedPassword = await generateCrptyPass(body.password);
            await this.userService.update(data.id, {
              password: hashedPassword,
            });
            statusCode = 202;
            resp = sendMethods({
              message: 'Senha trocada com sucesso',
              success: true,
            });

            body.hash && (await this.blackList.create(body.hash));
          } else {
            resp = sendMethods({
              message: 'Não é possível aplicar a mesma senha',
              success: false,
            });
          }
        } else {
          resp = sendMethods({
            message: 'Confirmação de senha não é igual a senha',
            success: false,
          });
        }
      } else {
        resp = sendMethods({
          message: 'Senha atual não confere',
          success: false,
        });
      }
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }

  @Post('forgot')
  async forgotPassword(
    @Body('email') email: string,
    @Res() response: Response,
  ) {
    let resp: send;
    let statusCode = 200;
    try {
      const user = await this.userService.findOne({ email });
      if (!user) {
        return response.status(200).send(
          sendMethods({
            message: 'Usuário não existe',
            success: false,
          }),
        );
      }

      const hash = await this.jwtService.signAsync({ id: user.id });

      const url = `http://seusite.com.br/usuario/trocar-senha?hash=${hash}`;

      const mailer = Mailer.useMailer();

      const success = await new Promise((resp, reject) => {
        mailer.verify((error) => {
          if (error) {
            reject(error);
          }
          const sendMail = {
            to: email,
            from: process.env.SEND_EMAIL,
            template: 'resetpass',
            subject: 'Esqueceu sua senha?',
            context: { url },
          };
          mailer.sendMail(sendMail, (error) => {
            if (error) {
              reject(error);
            }
            resp(null);
          });
        });
      })
        .then(() => true)
        .catch(() => false);

      if (success) {
        resp = sendMethods({
          message: 'Email enviado com sucesso',
          success: true,
        });
      } else {
        resp = sendMethods({
          message: 'Não foi possível enviar o email',
          success: false,
        });
      }
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }

  @Post('login')
  async login(
    @Body('login') login: string,
    @Body('password') password: string,
    @Res({ passthrough: true }) response: Response,
  ) {
    let resp: send;
    let statusCode = 200;
    try {
      const user = await this.userService.findLogin(login);

      if (!user) {
        return response.status(200).send(
          sendMethods({
            message: 'Usuário não existe',
            success: false,
          }),
        );
      }

      if (!(await compareCrptyPass(password, user.password))) {
        return response.status(200).send(
          sendMethods({
            message: 'Senha incorreta',
            success: false,
          }),
        );
      }

      const jwt = await this.jwtService.signAsync({ id: user.id });
      response.cookie('token', jwt, { httpOnly: true });

      resp = sendMethods({
        message: 'Usuário logado sucesso',
        success: true,
        data: { type: 'Bearer', token: jwt },
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e.detail,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }

  @Post('logout')
  async logout(@Res({ passthrough: true }) response: Response) {
    let resp: send;
    let statusCode = 200;
    try {
      response.clearCookie('token');
      resp = sendMethods({
        message: 'Usuário deslogado sucesso',
        success: true,
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }
}
