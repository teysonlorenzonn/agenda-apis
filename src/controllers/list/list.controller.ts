import {
  Body,
  Controller,
  Post,
  Put,
  Get,
  Delete,
  Req,
  Res,
  Param,
} from '@nestjs/common';
import { Response, Request } from 'express';
import { JwtService } from '@nestjs/jwt';

import { ListService } from 'src/services/list/list.service';
import { validJwt } from 'src/utils/funcs';
import { send, sendMethods } from 'src/utils/response';
import { ListProps, ItemListProps } from 'src/interfaces/list.interface';
import { omit } from 'ramda';

@Controller('list')
export class ListController {
  constructor(
    private readonly listService: ListService,
    private jwtService: JwtService,
  ) {}

  @Post()
  async createList(
    @Body() body: ListProps,
    @Res() response: Response,
    @Req() request: Request,
  ) {
    let statusCode = 200;
    let resp: send;
    try {
      let auth = request.headers['authorization'];
      if (!auth) {
        auth = request.cookies['token'];
      }
      const data = await validJwt(auth, this.jwtService);
      if (!data) {
        return response.status(401).send(
          sendMethods({
            message: 'Usuário não autorizado',
            success: false,
          }),
        );
      }
      const list = await this.listService.createList(body, data.id);

      resp = sendMethods({
        message: 'Lista criada com sucesso',
        success: true,
        data: omit(['creationDate', 'lastModifyDate'], list),
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }

  @Post('share')
  async shareList(
    @Body('listId') listId: string,
    @Res() response: Response,
    @Req() request: Request,
  ) {
    let statusCode = 200;
    let resp: send;
    try {
      let auth = request.headers['authorization'];
      if (!auth) {
        auth = request.cookies['token'];
      }
      const data = await validJwt(auth, this.jwtService);
      if (!data) {
        return response.status(401).send(
          sendMethods({
            message: 'Usuário não autorizado',
            success: false,
          }),
        );
      }
      await this.listService.shareList(listId, data.id);

      resp = sendMethods({
        message: 'Lista compartilhada com sucesso',
        success: true,
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }

  @Delete()
  async deleteList(
    @Body('id') listId: string,
    @Res() response: Response,
    @Req() request: Request,
  ) {
    let statusCode = 200;
    let resp: send;
    try {
      let auth = request.headers['authorization'];
      if (!auth) {
        auth = request.cookies['token'];
      }
      const data = await validJwt(auth, this.jwtService);
      if (!data) {
        return response.status(401).send(
          sendMethods({
            message: 'Usuário não autorizado',
            success: false,
          }),
        );
      }
      await this.listService.deleteList(listId, data.id);

      resp = sendMethods({
        message: 'Lista deletada com sucesso',
        success: true,
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }

  @Put()
  async updateList(
    @Body() body: ListProps,
    @Res() response: Response,
    @Req() request: Request,
  ) {
    let statusCode = 200;
    let resp: send;
    try {
      let auth = request.headers['authorization'];
      if (!auth) {
        auth = request.cookies['token'];
      }
      const data = await validJwt(auth, this.jwtService);
      if (!data) {
        return response.status(401).send(
          sendMethods({
            message: 'Usuário não autorizado',
            success: false,
          }),
        );
      }
      await this.listService.updateList(body);

      resp = sendMethods({
        message: 'Lista atualizada com sucesso',
        success: true,
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }

  @Put('item')
  async updateItemList(
    @Body() body: ItemListProps,
    @Res() response: Response,
    @Req() request: Request,
  ) {
    let statusCode = 200;
    let resp: send;
    try {
      let auth = request.headers['authorization'];
      if (!auth) {
        auth = request.cookies['token'];
      }
      const data = await validJwt(auth, this.jwtService);
      if (!data) {
        return response.status(401).send(
          sendMethods({
            message: 'Usuário não autorizado',
            success: false,
          }),
        );
      }
      await this.listService.updateItemList(body);

      resp = sendMethods({
        message: 'Item atualizado com sucesso',
        success: true,
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }

  @Post('item')
  async createItemList(
    @Body() body: ItemListProps,
    @Res() response: Response,
    @Req() request: Request,
  ) {
    let statusCode = 200;
    let resp: send;
    try {
      let auth = request.headers['authorization'];
      if (!auth) {
        auth = request.cookies['token'];
      }
      const data = await validJwt(auth, this.jwtService);
      if (!data) {
        return response.status(401).send(
          sendMethods({
            message: 'Usuário não autorizado',
            success: false,
          }),
        );
      }
      const item = await this.listService.createItemList(body);

      resp = sendMethods({
        message: 'Item criado com sucesso',
        success: true,
        data: omit(['creationDate', 'lastModifyDate'], item),
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }

  @Delete('item')
  async deleteItemList(
    @Body('id') id: string,
    @Res() response: Response,
    @Req() request: Request,
  ) {
    let statusCode = 200;
    let resp: send;
    try {
      let auth = request.headers['authorization'];
      if (!auth) {
        auth = request.cookies['token'];
      }
      const data = await validJwt(auth, this.jwtService);
      if (!data) {
        return response.status(401).send(
          sendMethods({
            message: 'Usuário não autorizado',
            success: false,
          }),
        );
      }
      await this.listService.deleteItemList(id);

      resp = sendMethods({
        message: 'Item deletado com sucesso',
        success: true,
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }

  @Get('all')
  async getAllLists(@Res() response: Response, @Req() request: Request) {
    let statusCode = 200;
    let resp: send;
    try {
      let auth = request.headers['authorization'];
      if (!auth) {
        auth = request.cookies['token'];
      }
      const data = await validJwt(auth, this.jwtService);
      if (!data) {
        return response.status(401).send(
          sendMethods({
            message: 'Usuário não autorizado',
            success: false,
          }),
        );
      }
      const lists = await this.listService.findAllLists(data.id);

      resp = sendMethods({
        message: 'Todas as listas',
        success: true,
        data: lists,
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }

  @Get(':id')
  async getList(
    @Param('id') id: string,
    @Res() response: Response,
    @Req() request: Request,
  ) {
    let statusCode = 200;
    let resp: send;
    try {
      let auth = request.headers['authorization'];
      if (!auth) {
        auth = request.cookies['token'];
      }
      const data = await validJwt(auth, this.jwtService);
      if (!data) {
        return response.status(401).send(
          sendMethods({
            message: 'Usuário não autorizado',
            success: false,
          }),
        );
      }
      const list = await this.listService.findOneList({ id });

      resp = sendMethods({
        message: 'Listas retornada com sucesso',
        success: true,
        data: omit(['creationDate', 'lastModifyDate'], list),
      });
    } catch (e) {
      statusCode = 400;
      resp = sendMethods({
        message: e,
        success: false,
      });
    }
    response.status(statusCode).send(resp);
  }
}
