import { UserController } from './user/user.controller';
import { AppController } from './app/app.controller';
import { ListController } from './list/list.controller';

export default [AppController, UserController, ListController];
