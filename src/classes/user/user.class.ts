import { UserProps } from 'src/interfaces/user.interface';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
import { Lists } from '../list/list.class';

@Entity('users')
export class User implements UserProps {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ unique: true })
  userName: string;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column({ nullable: true, type: 'bigint' })
  phoneNumber: number;

  @CreateDateColumn()
  creationDate: Date;

  @UpdateDateColumn()
  lastModifyDate: Date;

  @OneToMany(() => Lists, (list) => list.id)
  lists: Lists[];
}
