import { User } from './user/user.class';
import { ResetPasswordBlackList } from './blacklist/resetPasswordBlacklist.class';
import { Items } from './list/itemList.class';
import { Lists } from './list/list.class';
import { ListHasUsers } from './list/listHasUsers.class';

export default [User, ResetPasswordBlackList, Lists, Items, ListHasUsers];
