import { ListHasUsersProps } from 'src/interfaces/list.interface';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('listHasUsers')
export class ListHasUsers implements ListHasUsersProps {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('uuid')
  listId: string;

  @Column('uuid')
  userId: string;
}
