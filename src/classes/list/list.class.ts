import { ListProps } from 'src/interfaces/list.interface';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('lists')
export class Lists implements ListProps {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  description: string;

  @CreateDateColumn()
  creationDate: Date;

  @UpdateDateColumn()
  lastModifyDate: Date;
}
