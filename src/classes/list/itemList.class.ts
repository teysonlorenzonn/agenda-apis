import { ItemListProps } from 'src/interfaces/list.interface';
import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  JoinColumn,
} from 'typeorm';
import { Lists } from './list.class';

@Entity('items')
export class Items implements ItemListProps {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  quantity: number;

  @Column({ nullable: true })
  price: string;

  @CreateDateColumn()
  creationDate: Date;

  @UpdateDateColumn()
  lastModifyDate: Date;

  @Column('uuid')
  listId: string;

  @OneToMany(() => Lists, (item) => item.id, { cascade: true })
  @JoinColumn({ name: 'listId' })
  list: Lists;
}
